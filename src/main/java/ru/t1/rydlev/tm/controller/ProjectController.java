package ru.t1.rydlev.tm.controller;

import ru.t1.rydlev.tm.api.controller.IProjectController;
import ru.t1.rydlev.tm.api.service.IProjectService;
import ru.t1.rydlev.tm.model.Project;
import ru.t1.rydlev.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW PROJECTS]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project : projects) {
            final String name = project.getName();
            final String description = project.getDescription();
            System.out.printf("%s. %s : %s \n", index, name, description);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECT CLEAR]");
        projectService.deleteAll();
        System.out.println("[OK]");
    }

}
