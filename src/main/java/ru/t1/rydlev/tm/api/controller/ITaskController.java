package ru.t1.rydlev.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

}
