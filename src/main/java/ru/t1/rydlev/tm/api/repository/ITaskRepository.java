package ru.t1.rydlev.tm.api.repository;

import ru.t1.rydlev.tm.model.Project;
import ru.t1.rydlev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task task);

    void deleteAll();

}
