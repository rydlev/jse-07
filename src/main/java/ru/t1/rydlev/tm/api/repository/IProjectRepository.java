package ru.t1.rydlev.tm.api.repository;

import ru.t1.rydlev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void deleteAll();

}
