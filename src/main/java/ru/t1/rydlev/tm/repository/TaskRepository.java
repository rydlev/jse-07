package ru.t1.rydlev.tm.repository;

import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    public TaskRepository() {
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task add(Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void deleteAll() {
        tasks.clear();
    }

}
