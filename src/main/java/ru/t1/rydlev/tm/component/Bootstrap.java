package ru.t1.rydlev.tm.component;

import ru.t1.rydlev.tm.api.component.IBootstrap;
import ru.t1.rydlev.tm.api.controller.ICommandController;
import ru.t1.rydlev.tm.api.controller.IProjectController;
import ru.t1.rydlev.tm.api.controller.ITaskController;
import ru.t1.rydlev.tm.api.repository.ICommandRepository;
import ru.t1.rydlev.tm.api.repository.IProjectRepository;
import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.api.service.ICommandService;
import ru.t1.rydlev.tm.api.service.IProjectService;
import ru.t1.rydlev.tm.api.service.ITaskService;
import ru.t1.rydlev.tm.constant.ArgumentConstant;
import ru.t1.rydlev.tm.constant.TerminalConstant;
import ru.t1.rydlev.tm.controller.CommandController;
import ru.t1.rydlev.tm.controller.ProjectController;
import ru.t1.rydlev.tm.controller.TaskController;
import ru.t1.rydlev.tm.repository.CommandRepository;
import ru.t1.rydlev.tm.repository.ProjectRepository;
import ru.t1.rydlev.tm.repository.TaskRepository;
import ru.t1.rydlev.tm.service.CommandService;
import ru.t1.rydlev.tm.service.ProjectService;
import ru.t1.rydlev.tm.service.TaskService;
import ru.t1.rydlev.tm.util.TerminalUtil;

import java.util.Scanner;

public final class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskController taskController = new TaskController(taskService);

    @Override
    public void run(final String... args) {
        parseArguments(args);
        parseCommands();
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private void parseCommands() {
        commandController.showWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showArgumentError();
        }
        exit();
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConstant.HELP:
                commandController.showHelp();
                break;
            case TerminalConstant.VERSION:
                commandController.showVersion();
                break;
            case TerminalConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case TerminalConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConstant.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConstant.EXIT:
                exit();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void exit() {
        System.exit(0);
    }

}
