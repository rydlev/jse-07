package ru.t1.rydlev.tm.service;

import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.api.service.ITaskService;
import ru.t1.rydlev.tm.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        return taskRepository.add(task);
    }

    @Override
    public void deleteAll() {
        taskRepository.deleteAll();
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        return taskRepository.add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return taskRepository.add(task);
    }

}
